import { Component } from '@angular/core';
import { User } from './User';
import { UserService } from './user.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent {
  title = 'angularspinner';
  display = false;
  users: User[];
  constructor(private userService: UserService){
    this.fetchUsers = this.fetchUsers.bind(this);
  }
  ngOnInit() {
    this.display = true;
    setTimeout(this.fetchUsers, 2000);
  }
  fetchUsers() {
    this.userService
      .getUsers()
      .subscribe((data: User[]) => {
        this.users = data;
        this.display = false;
      });
    }
}
